# -*- Makefile -*-

arch = UNKNOWN

SUB_ARCH=$(shell echo `uname -m`)
ifeq ($(SUB_ARCH),x86_64)
arch=riscv64
endif

include hpl/Make.$(arch)

all: build_openblas build_vars
	- $(MKDIR) hpl/lib/$(arch)
	( $(CD) hpl/lib/arch/build ; $(MAKE) arch=$(arch) -f Makefile.hpcc )

clean:
	- $(MKDIR) hpl/lib/$(arch)
	( $(CD) hpl/lib/arch/build ; $(MAKE) arch=$(arch) -f Makefile.hpcc clean )

readme: README.html README.txt

README.html: README.tex
	hevea -fix -O README.tex
	python tools/readme.py README.html

README.info: README.tex
	hevea -fix -info README.tex

README.txt: README.tex
	hevea -fix -text README.tex

build_openblas:
	make -C $(TOPDIR)/hpsc-openblas HOSTCC=gcc TARGET=$(TARGET) NUM_THREADS=8 CC=riscv64-unknown-linux-gnu-clang FC=riscv64-unknown-linux-gnu-gfortran
	make -C $(TOPDIR)/hpsc-openblas HOSTCC=gcc TARGET=$(TARGET) NUM_THREADS=8 CC=riscv64-unknown-linux-gnu-clang FC=riscv64-unknown-linux-gnu-gfortran PREFIX=build install

build_vars:
	@echo "COMPILER:$(CC)" > build_vars.txt
	@echo "CFLAGS:$(CCFLAGS)" >> build_vars.txt

.PHONY: all clean readme build_vars